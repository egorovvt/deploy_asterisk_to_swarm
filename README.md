# deploy_asterisk_to_swarm

```mermaid
graph TD;
  start-->playbook[Playbook: Asterisk_deploy.yml];
  playbook-->pb1[hosts: swarm<br>roles: common];
  pb1-->pb2[hosts: swarm<br>roles: docker-swarm];
  pb2-->check1[проверка существующего класстера];
  check1-->create_new_swarm[host: leader<br>roles: init-swarm];
  check1-->deploy[host: swarm<br>role: deploy];
  create_new_swarm-->join-swarm-managers[host: managers<br>role: join-swarm-managers];
  create_new_swarm-->join-swarm-workers[host: managers<br>role: join-swarm-workers];
  join-swarm-workers-->deploy;
  join-swarm-managers-->deploy;
```


**roles_list:**
- _common_
  - install docker, git, etc 
- _docker-swarm_
  - Check existing docker_swarm, if true to skipping this role, else init next roles:
    - init-swarm
    - join-swarm-managers
    - join-swarm-workers
- _init-swarm_
  - create new docker_swarm, on _leader_ host
- _join-swarm-managers_
  - take all hosts in group _managers_ and join to swarm
- _join-swarm-workers_
  - take all hosts in group _wokrers_ and join to swarm
